import 'bootstrap';
import './scss/styles.scss';

import React , {Fragment} from "react";
import ReactDOM from "react-dom";
import App from "./components/app";

ReactDOM.render(<App/>, document.getElementById('app'));