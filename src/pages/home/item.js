import React , {Fragment, Component} from "react";
import {Card, CardBody, CardTitle, CardSubtitle,
Modal, ModalBody, ModalHeader, ModalFooter, Button,
Form, FormGroup, Label, Input, FormText,
Container, Row, Col} from "reactstrap";
import Badge from "reactstrap/es/Badge";
import UserInfo from "./userInfo";
import {Spinner} from "reactstrap/dist/reactstrap.es";

export default class Item extends Component {
    constructor(props) {
        super(props);

        this.state = {
            value: '',
            showModalRemove: false,
            showModalStatus: false,
            showModalUpdate: false,
            showModalUserInfo: false,
            userInfo: {},
        }
    }

    render() {
        let {item} = this.props;
        let {showModalRemove, showModalStatus, showModalUpdate, showModalUserInfo, userInfo} = this.state;
        return (
            <Col lg={12}>
                <Card>
                    <CardBody>
                        <CardTitle>#{item.id} {item.title}</CardTitle>
                        <CardSubtitle className="item__subtitle" onClick={(e) => this.toggleModalUserInfo(e,item.userId)}>
                            Created by user #{item.userId}
                        </CardSubtitle>
                        <Row>
                            <Col lg={6} md={6} sm={6} xs={6}>
                                <Row>
                                    <Col lg={3} md={3} sm={4} xs={12}>
                                        <Badge onClick={this.toggleModalStatus} color={item.completed ? 'success' : 'warning'}>
                                            {item.completed ? 'completed' : 'uncompleted'}
                                        </Badge>
                                    </Col>
                                    <Col lg={3} md={3} sm={4} xs={12}>
                                        <Badge onClick={(e) => this.toggleModalUpdate(e, item.title)} color='primary'>
                                            update
                                        </Badge>
                                    </Col>
                                </Row>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={6} className="taRight">
                                <Badge onClick={this.toggleModalRemove} color='danger'>
                                    remove
                                </Badge>
                            </Col>
                        </Row>
                    </CardBody>
                    <Modal isOpen={showModalRemove} toggle={this.toggleModalRemove}>
                        <ModalHeader toggle={this.toggleModalRemove}>Удалить текущий таск?</ModalHeader>
                        <ModalBody>
                            Хотите удалить таск #{item.id}?
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary" onClick={() => this.removeItem(item.id)}>Удалить</Button>
                            <Button color="secondary" onClick={this.toggleModalRemove}>Отмена</Button>
                        </ModalFooter>
                    </Modal>


                    <Modal isOpen={showModalStatus} toggle={this.toggleModalStatus}>
                        <ModalHeader toggle={this.toggleModalStatus}>Меняем статус текущего таска?</ModalHeader>
                        <ModalFooter>
                            <Button color="primary" onClick={() => this.toggleStatus(item.id)}>Да, меняем</Button>
                            <Button color="secondary" onClick={this.toggleModalStatus}>Отмена</Button>
                        </ModalFooter>
                    </Modal>


                    <Modal isOpen={showModalUpdate} toggle={this.toggleModalUpdate}>
                        <ModalHeader toggle={this.toggleModalUpdate}>Изменить таск</ModalHeader>
                        <ModalBody>
                            <Form>
                                <FormGroup>
                                    <Label for="taskName">Изменить таск #{item.id}</Label>
                                    <Input type="text" name="taskName" id="taskName" onChange={(e) => this.setValue(e)}
                                           value={this.state.value}/>
                                </FormGroup>
                            </Form>
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary" onClick={() => this.updateTask(item.id)}>Ок</Button>
                            <Button color="secondary" onClick={this.toggleModalUpdate}>Отмена</Button>
                        </ModalFooter>
                    </Modal>

                    <Modal isOpen={showModalUserInfo} toggle={this.toggleModalUserInfo}>
                        <ModalHeader toggle={this.toggleModalUserInfo}>Информация о пользователе
                            #{item.userId}</ModalHeader>
                        <ModalBody>
                            {Object.keys(userInfo).length?
                                <UserInfo userInfo={userInfo}/>:
                                <Spinner color="primary"/>
                            }
                        </ModalBody>
                    </Modal>
                </Card>
            </Col>
        );
    }

    toggleModalRemove = () => {
        this.setState({
            showModalRemove: !this.state.showModalRemove
        })
    }

    toggleModalStatus = () => {
        this.setState({
            showModalStatus: !this.state.showModalStatus
        })
    }

    toggleModalUpdate = (e, inputValue = '') => {
        inputValue = inputValue.trim();
        if (!this.state.showModalUpdate && inputValue.length) {
            this.setState({
                showModalUpdate: !this.state.showModalUpdate,
                value: inputValue
            })
        } else {
            this.setState({
                showModalUpdate: !this.state.showModalUpdate,
            })
        }
    }

    toggleModalUserInfo = (e, id) => {
        if (!this.state.showModalUserInfo) {
            fetch(`https://jsonplaceholder.typicode.com/users/${id}`)
                .then(user => user.json())
                .then(user => {
                    this.setState({userInfo: user, showModalUserInfo: !this.state.showModalUserInfo})
                });
        } else {
            this.setState({showModalUserInfo: !this.state.showModalUserInfo});
        }
    }

    removeItem = (id) => {
        this.props.remove(id);
        this.setState({
            showModalRemove: !this.state.showModalRemove
        })
    }

    toggleStatus = (id) => {
        this.props.toggleStatus(id);
        this.setState({
            showModalStatus: !this.state.showModalStatus
        })
    }

    setValue = (e) => {
        this.setState({
            value: e.target.value
        })
    }

    updateTask = (id) => {
        this.props.update(id, this.state.value);
        this.setState({
            showModalUpdate: !this.state.showModalUpdate,
        })
    }
}