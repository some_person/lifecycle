import './scss/styles.scss';
import iconDesc from './images/icon-desc.png';
import iconAsc from './images/icon-asc.png';
import iconAlpha from './images/icon-alpha.png';

import React , {Fragment, Component} from "react";
import {Card, CardBody, CardTitle, Spinner,
Modal, ModalBody, ModalHeader, ModalFooter, Button,
Form, FormGroup, Label, Input, FormText} from "reactstrap";
import {Container, Row, Col} from "reactstrap/dist/reactstrap.es";
import Item from "./item";
import Users from "./usersSelect";
import Search from "./search";

export default class Content extends Component{

    constructor(props) {
        super(props);
        this.state = {
            items: [],
            itemsSearch: [],
            showModalAdd: false,
            taskName: '',
            users: [],
            addTaskUserId: 1,
            sort: "desc",
            icon: "desc",
            orderBy: "id",
        }
    }

    render() {
        let {items, showModalAdd, users, itemsSearch, icon} = this.state;
        let showItems = itemsSearch.length ? itemsSearch : items;
        console.log(itemsSearch);
        return(
            <main>
                <Container>
                    {items.length ?
                        <Fragment>
                            <Row>
                                <Col lg={12}><Button color="info" className="topBtn" onClick={this.toggleModalAdd}>Добавить новый таск</Button>
                                <Button color="info" className="topBtn" onClick={(e) => this.toggleSort(e, "id")}>Сортировать по ID<span className="sortIcon"><img
                                    src={icon === "desc" ? iconDesc : iconAsc} alt=""/></span></Button>
                                <Button color="info" className="topBtn" onClick={(e) => this.toggleSort(e, "name")}>Сортировать по названию <a href="#!" className="sortIcon"><img
                                    src={iconAlpha} alt=""/></a></Button>
                                </Col>
                            </Row>
                            <Row>
                                <Col lg={12}>
                                    <Search items={this.state.items} search={this.searchItems}/>
                                </Col>
                            </Row>
                            <Row>
                            {showItems.map(item => {
                                return <Item key={item.id} item={item} remove={this.removeItemFromItems} toggleStatus={this.toggleStatus} update={this.updateTask} getUsers={this.getUsers}/>
                        })}
                            </Row>
                        </Fragment>
                        :
                        <Spinner color="primary" />
                    }
                </Container>

                <Modal isOpen={showModalAdd} toggle={this.toggleModalAdd}>
                    <ModalHeader toggle={this.toggleModalAdd}>Добавить таск?</ModalHeader>
                    <ModalBody>
                        <Form>
                            <FormGroup>
                                <Label for="newTaskName">Введите имя таска</Label>
                                <Input type="text" name="newTaskName" id="newTaskName" onChange={(e) => this.setTaskName(e)} value={this.state.taskName}/>
                            </FormGroup>
                                {users.length ?
                                    <Users users={users} addTaskSetUserId={this.addTaskSetUserId}/>:
                                    <Spinner/>
                                }
                        </Form>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={() => this.addTask()}>Ок</Button>
                        <Button color="secondary" onClick={this.toggleModalAdd}>Отмена</Button>
                    </ModalFooter>
                </Modal>
            </main>
        );
    }

    componentDidMount() {
        fetch('https://jsonplaceholder.typicode.com/todos')
            .then(items => items.json())
            .then(items => {
                this.setState(
                    {
                        items: [...items]
                    }
                );
            });
    }

    searchItems = (searchValue) => {
        this.setState({
            itemsSearch: searchValue.length >= 3 ?
                this.state.items.filter(item => {
                    let regexp = new RegExp(`\^${searchValue}`, "i");
                    let matches = regexp.test(item.title);
                    if (matches) {
                        return item;
                    }
                }) : []
        })
        console.log(searchValue.length);
    }

    toggleSort = (e, orderBy) => {
        this.setState({
            sort: this.state.sort === "desc" ? "asc" : "desc",
        });
        if (orderBy === "id") {
            this.sortTasksById()
            this.setState({
                icon: this.state.icon === "desc" ? "asc" : "desc",
            });
        } else {
            this.sortTasksByName()
        }
    }

    sortTasksById = () => {
        let itemsForSort = this.state.itemsSearch.length ? this.state.itemsSearch : this.state.items;
        itemsForSort.sort((a, b) => {
            if (this.state.sort === "asc") {
                return a.id - b.id
            } else if (this.state.sort === "desc") {
                return b.id - a.id
            }
        });
        if (this.state.itemsSearch.length) {
            this.setState({itemsSearch: itemsForSort})
        } else {
            this.setState({items: itemsForSort})
        }
    }

    sortTasksByName = () => {
        console.log(this.state.items);
        let sort = this.state.sort;
        let itemsForSort = this.state.itemsSearch.length ? this.state.itemsSearch : this.state.items;
        itemsForSort.sort(function (a, b) {
            if (a.title > b.title) {
                return sort === "asc" ? 1 : -1;
            }
            if (a.title < b.title) {
                return sort === "asc" ? -1 : 1;
            }
            return 0;
        });
        if (this.state.itemsSearch.length) {
            this.setState({itemsSearch: itemsForSort})
        } else {
            this.setState({items: itemsForSort})
        }
    }

    removeItemFromItems = (id) => {
        this.setState({
            items: this.state.items.filter(item => item.id !== id),
            itemsSearch: this.state.itemsSearch.length ? this.state.itemsSearch.filter(item => item.id !== id) : []
        })
    }

    toggleStatus = (id) => {
        this.setState({
            items: this.state.items.map(item => {
                if (item.id === id) {
                    item.completed = !item.completed;
                }
                return item;
            })
        })
    }

    updateTask = (id, value) => {
        this.setState({
            items: this.state.items.map(item => {
                if (item.id === id) {
                    item.title = value;
                }
                return item;
            })
        })
    }

    toggleModalAdd = () => {
        this.getUsers()
            .then(users => {
                this.setState({users: users})
            })
        this.setState({
            showModalAdd: !this.state.showModalAdd,
            taskName: 'My new task name',
        })
    }

    setTaskName = (e) => {
        this.setState({
            taskName: e.target.value
        })
    }

    addTask = () => {
        console.log(this.state.addTaskUserId)

        let task = {
            "id" : this.state.items[this.state.items.length - 1].id + 1,
            "userId" : this.state.addTaskUserId,
            "title": this.state.taskName,
            "completed": false
        };
        this.setState({
            items: [...this.state.items, task],
            showModalAdd: !this.state.showModalAdd
        })
    }

    getUsers = () => {
      return fetch("https://jsonplaceholder.typicode.com/users")
            .then(users => users.json());
    }

    addTaskSetUserId = (userId) => {
        this.setState({
            addTaskUserId: userId
        })
    }
}
