import React , {Fragment, Component} from "react";
import {Card, CardBody, CardTitle, Spinner,
    Modal, ModalBody, ModalHeader, ModalFooter, Button,
    Form, FormGroup, Label, Input, FormText} from "reactstrap";

export default class usersSelect extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let {users} = this.props;
        return(
            <FormGroup>
                <Label for="select">Select user</Label>
                <Input type="select" name="select" id="select" multiple>
                    {
                        users.map(user => {
                            return <option key={user.id} onClick={() => this.setUserId(user.id)}>{user.username}</option>
                        })
                    }
                </Input>
            </FormGroup>
        )
    }

    setUserId = (id) => {
        console.log(id);
        this.props.addTaskSetUserId(id);
    }
}