import React , {Fragment, Component} from "react";
import {Form, FormGroup,Input} from "reactstrap";


export default class search extends Component {
    constructor(props) {
        super(props);

        this.state = {
            searchValue: '',
            searchItems: []
        }
    }

    render() {
        return (
            <Form>
                <FormGroup>
                    <Input value={this.state.searchValue}  onChange={(e)=>this.handleChange(e)} type="text" name="searchTask" id="searchTask" placeholder="найти запись по названию (минимум 3 символа)" />
                </FormGroup>
            </Form>
        );
    }

    handleChange = (e) => {

        this.setState({searchValue: e.target.value});

        this.props.search(e.target.value);
    }
}