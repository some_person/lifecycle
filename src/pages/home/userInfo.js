import React , {Fragment, Component} from "react";
import {Card, CardBody, CardTitle, CardSubtitle,
    Modal, ModalBody, ModalHeader, ModalFooter, Button,
    Form, FormGroup, Label, Input, FormText,
    Container, Row, Col} from "reactstrap";
import Badge from "reactstrap/es/Badge";

export default class userInfo extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        let {userInfo} = this.props;

        return(
            <Fragment>
                <Row>
                    <Col lg={12}><Badge color="info">Name:</Badge> {userInfo.name}</Col>
                    <Col lg={12}><Badge color="info">Username:</Badge> {userInfo.username}</Col>
                    <Col lg={12}><Badge color="info">Email:</Badge> {userInfo.email}</Col>
                    <Col lg={12}><Badge color="info">Address:</Badge> </Col>
                        <Col lg={{size: 11, offset: 1}} className="address__item">
                            <Badge color="primary">Street:</Badge> {userInfo.address.street}
                        </Col>
                        <Col lg={{size: 11, offset: 1}} className="address__item">
                            <Badge color="primary">Suite:</Badge> {userInfo.address.suite}
                        </Col>
                        <Col lg={{size: 11, offset: 1}} className="address__item">
                            <Badge color="primary">City:</Badge> {userInfo.address.city}
                        </Col>
                    <Col lg={12}><Badge color="info">Phone:</Badge> {userInfo.phone}</Col>
                    <Col lg={12}><Badge color="info">Company:</Badge> {userInfo.company.name}</Col>
                </Row>
            </Fragment>
        )
    }
}