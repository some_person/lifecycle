import React , {Fragment, Component} from "react";

import Content from "./../pages/home";

export default class App extends Component{
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <Fragment>
                <Content/>
            </Fragment>
        );
    }
}